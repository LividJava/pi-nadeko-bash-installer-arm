<font size=4>**THIS IS THE PI VERSION AND INSTRUCTIONS ARE COPIED FROM THE ORIGINAL NADEKO GUIDE**</font>

<font size=3>If you have any issues with the installer, please join the discord server **[here](https://discord.gg/vjkvnaWGXQ).**</font>
<hr/>

[[_TOC_]]

## Linux From Source

**Note: Only the RPi 4B with Ubuntu has been tested.**

Open Terminal (if you're on an installation with a window manager) and navigate to the location where you want to install the bot (for example `cd ~`) 

##### Installation Instructions

1. Download and run the **new** installer script `cd ~ && wget -N https://gitlab.com/hokutochen/pi-nadeko-bash-installer-arm/-/raw/V4/linuxAIO.sh && bash linuxAIO.sh`
2. Install prerequisites (type `1` and press enter)
3. Download the bot (type `2` and press enter)
4. Exit the installer in order to set up your `creds.yml` 
5. Copy the creds.yml template `cp nadekobot/output/creds_example.yml nadekobot/output/creds.yml` 
6. Open `nadekobot/output/creds.yml` with your favorite text editor. We will use nano here
    - `nano nadekobot/output/creds.yml`
7. [Enter your bot's token](https://nadekobot.readthedocs.io/en/latest/creds-guide/)
    - After you're done, you can close nano (and save the file) by inputting, in order 
       - <kbd>^Ctrl + X</kbd>
       - <kbd>Y</kbd>
       - <kbd>⏎ Enter</kbd>
8. Run the bot (type `3` and press enter)

    8a. **Note: If you encounter a `dotnet is not installed` error, exit the installer and copy and paste each line**
    <details open>
    <summary>dotnet fix</summary>
    
    ```sh
    export DOTNET_ROOT=/usr/share/dotnet-arm64
    export PATH=$PATH:/usr/share/dotnet-arm64
    ```
    </details>
    
    Ensure that these lines are placed somewhere within your `.bashrc` file

    <details>
    <summary>Outdated Method</summary>

    ***DO not do the method below since it does not work at the moment***

    **Note: If you encounter a `dotnet is not installed` error, exit the installer and copy and paste the code block below:**
    ```
    sudo echo '# dotnet core wrapper' >> .bashrc &&
    sudo echo 'export DOTNET_ROOT=/usr/share/dotnet-arm64' >> .bashrc &&
    sudo echo 'export PATH=$PATH:/usr/share/dotnet-arm64' >> .bashrc
    ```
    </details>
<br/>

9. Restart the pi or server.
10. Rerun the installer and try starting the bot. 

##### Update Instructions

1. ⚠ Stop the bot
2. Update and run the **new** installer script `cd ~ && wget -N https://gitlab.com/hokutochen/pi-nadeko-bash-installer-arm/-/raw/V4/linuxAIO.sh && bash linuxAIO.sh`
3. Update the bot (type `2` and press enter)
4. Run the bot (type `3` and press enter)
5. 🎉 

## Running Nadeko

While there are two run modes built into the installer, these options only run Nadeko within the current session. Below are 3 methods of running Nadeko as a background process.

### Tmux (Preferred Method)

Using `tmux` is the simplest method, and is therefore recommended for most users.

1. Start a tmux session:
    - `tmux`
2. Navigate to the project's root directory
    - Project root directory location example: `/home/user/nadekobot/`
3. Enter the `output` directory:
    - `cd output`
4. Run the bot using:
    - `dotnet NadekoBot.dll`
5. Detatch the tmux session:
    - Press <kbd>^Ctrl + B</kbd>
    - Then press <kbd>D</kbd>

Nadeko should now be running in the background of your system. To re-open the tmux session to either update, restart, or whatever, execute `tmux a`.

If you'd like to have this start on system starup see [here](#systemd-with-tmux).

### Systemd

Compared to using tmux, this method requires a little bit more work to set up, but has the benefit of allowing Nadeko to automatically start back up after a system reboot or the execution of the `.die` command.

1. Navigate to the project's root directory
    - Project root directory location example: `/home/user/nadekobot/`
2. Use the following command to create a service that will be used to start Nadeko:

    ```bash
    echo "[Unit]
    Description=NadekoBot service
    After=network.target
    StartLimitIntervalSec=60
    StartLimitBurst=2
    
    [Service]
    Type=simple
    User=$USER
    WorkingDirectory=$PWD/output
    # If you want Nadeko to be compiled prior to every startup, uncomment the lines
    # below. Note  that it's not neccessary unless you are personally modifying the
    # source code.
    #ExecStartPre=/usr/bin/dotnet-arm64/dotnet build ../src/NadekoBot/NadekoBot.csproj -c Release -o output/
    ExecStart=/usr/bin/dotnet-arm64/dotnet NadekoBot.dll
    Restart=on-failure
    RestartSec=5
    StandardOutput=syslog
    StandardError=syslog
    SyslogIdentifier=NadekoBot
    
    [Install]
    WantedBy=multi-user.target" | sudo tee /etc/systemd/system/nadeko.service
    ```
    
3. Make the new service available:
    - `sudo systemctl daemon-reload`
4. Start Nadeko:
    - `sudo systemctl enable --now nadeko.service`

Additionally to check logs run `sudo systemctl status nadeko` this will show you the last few lines of the output log.


### Systemd with tmux
This method of starting Nadeko works in much the same fashion as above, but rather than outputing the console output to `sudo systemctl status nadeko` you can instead live view the console from a tmux session.

You'll notice a lot of the steps are similar but changed to accommodate tmux.

1. Navigate to the project's root directory
    - Project root directory location example: `/home/user/nadekobot/`
2. Use the following command to create a service that will be used to start Nadeko:

    ```bash
    echo "[Unit]
    Description=NadekoBot service
    After=network.target
    StartLimitIntervalSec=60
    StartLimitBurst=2
    
    [Service]
    Type=simple
    User=$USER
    WorkingDirectory=$PWD/output
    # If you want Nadeko to be compiled prior to every startup, uncomment the lines
    # below. Note  that it's not neccessary unless you are personally modifying the
    # source code.
    #ExecStartPre=/usr/bin/dotnet-arm64/dotnet build ../src/NadekoBot/NadekoBot.csproj -c Release -o output/
    ExecStart=/usr/bin/tmux new-session -s nadeko -d '/usr/bin/dotnet-arm64/dotnet NadekoBot.dll
    ExecStop=/bin/sleep 2
    
    [Install]
    WantedBy=multi-user.target" | sudo tee /etc/systemd/system/nadeko.service
    ```
3. Make the new service available:
    - `sudo systemctl daemon-reload`
4. Start Nadeko:
    - `sudo systemctl enable --now nadeko.service`

Much like the tmux example above to access the console
 - `tmux a -t Nadeko`

To exit press <kbd>^Ctrl + B</kbd> followed by <kbd>D</kbd>
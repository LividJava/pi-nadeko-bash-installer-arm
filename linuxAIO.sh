#!/bin/bash
# Just incase someone decides to use this on a non ARM syste,
if [ "$(uname -m)" != "aarch64" ]
 then
  echo "This doesnt appear to be an ARM based system, Use the Main script instead"
  echo "If you believe this message is in error join the Nadeko ARM/Pi Server: https://discord.gg/vjkvnaWGXQ"
  exit 1
else
  echo "Java was here"
  clear
fi

echo ""
echo "Welcome to NadekoBot for ARM/Raspberry pi. Downloading the latest installer..."
echo "If you encounter any issues with this script join the NadekoBot Pi/ARM discord here: https://discord.gg/vjkvnaWGXQ"
root=$(pwd)

export DOTNET_ROOT=/usr/share/dotnet-arm64
export PATH=$PATH:/usr/share/dotnet-arm64


rm "$root/n-menu.sh" 1>/dev/null 2>&1
wget -N https://gitlab.com/hokutochen/pi-nadeko-bash-installer-arm/-/raw/V4/n-menu.sh

bash n-menu.sh
cd "$root"
rm "$root/n-menu.sh"
exit 0
